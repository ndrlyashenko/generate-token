public class TokenStatus{
	public static final String LIMITED = 'limited';
	public static final String UNLIMITED = 'unlimited';
	public static final String INVALID = 'invalid';
	public static final String DATE_EXPIRED = 'date_expired';
}