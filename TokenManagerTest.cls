@isTest
private class TokenManagerTest {

	@testSetup
	static void setup(){
		Encryption_Settings__c settings = new Encryption_Settings__c(
			Name = 'Private Key',
			Value__c = 'ZcQWFbdB/OdI3nN9ioEX+Q==');
		insert settings;
	}

	@isTest
	static void testGenerateToken(){
		
		final String generatedToken = TokenManager.generateToken(
			new Map<String, Object>{
				'expiration_date' => Date.newInstance(2017, 9, 19),
				'organization_id' => 'org_id',
				'package_prefix' => 'pre',
				'license_type' => 'Unlimited'
			});
		System.assert(generatedToken != null);
	}

	@isTest
	static void testObtainTokenStatusUnlimited(){

		final String generatedToken = TokenManager.generateToken(
			new Map<String, Object>{
				'expiration_date' => Date.newInstance(2017, 9, 19),
				'organization_id' => 'org_id',
				'package_prefix' => 'pre',
				'license_type' => 'Unlimited'
			});
		System.assert(generatedToken != null);

		final String status = TokenManager.obtainTokenStatus(
			new Map<String, Object>{
				'token' => generatedToken
			});
		System.assertEquals(TokenStatus.UNLIMITED, status);
	}

	@isTest
	static void testObtainTokenStatusLimited(){

		final String generatedToken = TokenManager.generateToken(
			new Map<String, Object>{
				'expiration_date' => Date.newInstance(2017, 9, 19),
				'organization_id' => 'org_id',
				'package_prefix' => 'pre',
				'license_type' => 'Limited'
			});
		System.assert(generatedToken != null);		

		final String status = TokenManager.obtainTokenStatus(
			new Map<String, Object>{
				'token' => generatedToken
			});
		System.assertEquals(TokenStatus.LIMITED, status);
	}


	@isTest
	static void testObtainTokenStatusNoToken(){				
		final String status = TokenManager.obtainTokenStatus(
			new Map<String, Object>{
			});
		System.assertEquals(TokenStatus.INVALID, status);
	}
    
    @isTest
	static void testObtainTokenStatusInvalidToken(){				
		final String status = TokenManager.obtainTokenStatus(
			new Map<String, Object>{
                'token' => 'ptnpYVicpKMk817DzwylGcFHKx+X5MIeJ+peh5Qy5tKmRaqn34rTQnfD4IyPKZ8fHbVNkTMg=='
			});
		System.assertEquals(TokenStatus.INVALID, status);
	}

	@isTest
	static void testObtainTokenStatusExpiredDate(){

		final String generatedToken = TokenManager.generateToken(
			new Map<String, Object>{
				'expiration_date' => Date.newInstance(2011, 9, 19),
				'organization_id' => 'org_id',
				'package_prefix' => 'pre',
				'license_type' => 'Limited'
			});
		System.assert(generatedToken != null);

		final String status = TokenManager.obtainTokenStatus(
			new Map<String, Object>{
				'token' => generatedToken
			});
		System.assertEquals(TokenStatus.DATE_EXPIRED, status);
	}
}