@RestResource(urlMapping='/v1/token/status')
global class TokenStatusResource  {

    @HttpPost
    global static void HttpPost(String token) {            
        if(String.isBlank(token)){            
            return;
        }

        final String tokenStatus = TokenManager.obtainTokenStatus(new Map<String, Object>{
            'token' => token
        });
        
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf('{"status" : "' + tokenStatus + '"}');
    }
}