public class TokenManager {
	
	/**
	 * Create token encrypting data: expiration date, org id, package prfix, license type
	 * (year+'/'+month+'/'+day+' ID '+organizationId+' PR '+packagePrefix+' TP '+licenseType),
	 * using the specified algorithm 'AES128' and private key from Encryption_Settings__c
	 * 
	 * @param  packageInfo  data to encrypt
	 * @return public generated token
	 */
	public static String generateToken(Map<String, Object> packageInfo){

		final Date expirationDate = (Date) packageInfo.get('expiration_date');
		final String organizationId = (String) packageInfo.get('organization_id');
		final String packagePrefix = (String) packageInfo.get('package_prefix');
		final String licenseType = (String) packageInfo.get('license_type');

		final Blob encryptedPackageInfo = Crypto.encryptWithManagedIV(
			'AES128', 
			EncodingUtil.base64Decode(Encryption_Settings__c.getInstance('Private Key').Value__c), 
			Blob.valueOf(expirationDate.year()+'/'+expirationDate.month()+'/'+expirationDate.day()+' ID '+organizationId+' PR '+packagePrefix+' TP '+licenseType));

		final String token = EncodingUtil.base64Encode(encryptedPackageInfo);
		System.debug('token: ' + token);

		return token;
	}

	/**
	 * Decrypt token
	 * Return invalid status, if token length is not multiple of 16.
	 * Return expired date status, if token expiration date < today date.
	 * Return unlimited status, if token contains 'Unlimited',
	 * 			otherwise return limited status.
	 * 					
	 * @param  tokenInfo
	 *         must contain token
	 * @return token status
	 *               [invalid
	 *               date_expired
	 *               limited
	 *               unlimited]
	 */
	public static String obtainTokenStatus(Map<String, Object> tokenInfo){
		System.debug(tokenInfo);
        
		final String token = (String) tokenInfo.get('token');
        if(String.isBlank(token)){
			return TokenStatus.INVALID;	
        }
        final Blob decodedToken = EncodingUtil.base64Decode(token);
        if(Math.mod(decodedToken.size(), 16) > 0){
			return TokenStatus.INVALID;	            
        }
        
        final String packageInfo = Crypto.decryptWithManagedIV(
        	'AES128', 
        	EncodingUtil.base64Decode(Encryption_Settings__c.getInstance('Private Key').Value__c), 
        	EncodingUtil.base64Decode(token)
        ).toString();
		System.debug('encrypted token: ' + packageInfo);

		final String[] expirationDateStr = packageInfo.split(' ID ');
		if(expirationDateStr.size() < 1){
			return TokenStatus.INVALID;			
		}
		final String[] expirationDateInParts = expirationDateStr[0].split('/');
		if(expirationDateInParts.size() < 3){
			return TokenStatus.INVALID;			
		}
		final Date expirationDate = Date.newInstance(
			Integer.valueOf(expirationDateInParts[0]), 
			Integer.valueOf(expirationDateInParts[1]), 
			Integer.valueOf(expirationDateInParts[2])
		);
		if(expirationDate < Date.today()){
			return TokenStatus.DATE_EXPIRED;
		}

		return packageInfo.contains('Unlimited') ? TokenStatus.UNLIMITED : TokenStatus.LIMITED;
	}

}